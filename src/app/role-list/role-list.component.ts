import { Component, OnInit } from '@angular/core';
import { Role } from '../entity/Role';
import { RoleService } from '../role.service';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit{

  constructor(private service:RoleService){}


    roles:Array<Role> = [
      {
        "id":1,
        "name":"Admin"
      },
      {
        "id":2,
        "name":"Oreia"
      }
    ];

    public getRoles(){
      this.service.getRoles().subscribe(
        Response => this.roles = Response
      );
    }

    ngOnInit() {
      this.getRoles();
    }
}
