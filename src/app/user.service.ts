import { Injectable } from '@angular/core';
import { User } from './entity/User';
import { Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = 'api/users';

  constructor(private http: HttpClient) {}

  getUsers() : Observable<User[]> {
    return this.http.get<User[]>(this.url);
  }

  setUser(user:User):Observable<any>{
    return this.http.post(this.url, user);

  }
}
