import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public value:number;
  public value2:number;
  public total:number;

  public somar() {
    this.total = this.value + this.value2;
  }
}