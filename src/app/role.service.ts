import { Injectable } from '@angular/core';
import { Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Role } from './entity/Role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private url = 'api/roles';

  constructor(private http: HttpClient) {}

  getRoles() : Observable<Role[]> {
    return this.http.get<Role[]>(this.url);
  }

  setRole(role:Role):Observable<any>{
    return this.http.post(this.url, role);

  }
}
