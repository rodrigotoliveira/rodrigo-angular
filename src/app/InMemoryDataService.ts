import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const roles = [
      { id: 1, name: 'Admin' },
      { id: 2, name: 'Oreia' },
      { id: 3, name: 'Aluno' }
    ];
    const users = [
        { id: 1, name: 'Rodrigo' },
        { id: 2, name: 'Rafael' },
        { id: 3, name: 'João' }
      ];
    return {roles,users};
  }
}