import { Component, OnInit } from '@angular/core';
import { RoleService } from '../role.service';
import { Router } from '../../../node_modules/@angular/router';
import { Role } from '../entity/Role';

@Component({
  selector: 'app-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.css']
})
export class RoleFormComponent implements OnInit {

  role:Role = new Role();

  constructor(protected service:RoleService, private router:Router) {   
  }

  salvar(){
    this.service.setRole(this.role).subscribe(
     Response => this.router.navigate(['/role']) 
    );
  }

  ngOnInit() {
  }

}
