import { Component, OnInit } from '@angular/core';
import { User } from '../entity/User';
import { Role } from '../entity/Role';
import { Router } from '../../../node_modules/@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  user : User = new Role();
  constructor(protected service:UserService, private router:Router) { }

  salvar(){
    this.service.setUser(this.user).subscribe(
     Response => this.router.navigate(['/user']) 
    );
  }

  ngOnInit() {
  }

}
