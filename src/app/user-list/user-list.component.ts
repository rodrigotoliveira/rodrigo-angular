import { Component, OnInit } from '@angular/core';
import { User } from '../entity/User';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private service:UserService){}

  users:Array<User> = [
    {
      "id":1,
      "name":"Rodrigo"
    },
    {
      "id":2,
      "name":"Rafael"
    }
  ];

  public getUsers(){
      this.service.getUsers().subscribe(
      Response => this.users = Response
    );
  }

  ngOnInit() {
    this.getUsers();
  }
}
